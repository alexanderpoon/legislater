-- Takes files created with assemble_data.sh and creates a sqlite db with each file as a table

.mode csv bill_actions

.import /Users/alexanderpoon/Desktop/R/legislateR/data/bill_actions.csv bill_actions
.import /Users/alexanderpoon/Desktop/R/legislateR/data/bill_legislator_votes.csv bill_legislator_votes
.import /Users/alexanderpoon/Desktop/R/legislateR/data/bill_sponsors.csv bill_sponsors
.import /Users/alexanderpoon/Desktop/R/legislateR/data/bill_votes.csv bill_votes
.import /Users/alexanderpoon/Desktop/R/legislateR/data/bills.csv bills
.import /Users/alexanderpoon/Desktop/R/legislateR/data/committees.csv committees
.import /Users/alexanderpoon/Desktop/R/legislateR/data/legislator_roles.csv legislator_roles
.import /Users/alexanderpoon/Desktop/R/legislateR/data/legislators.csv legislators
