#!/bin/bash

# A script to scrape full bill text pdfs from the TN General Assembly's website
# (http://www.capitol.tn.gov/)
# I installed wget via homebrew (https://brew.sh/) in order to run this

# Bill pdfs will be output in the following directory
cd ~/Desktop/R/legislateR/data/bills_pdf
mkdir {105..110}th

# 110th Legislative Session (continues through 2018)
cd 110th

for i in {1..1462}
do
    printf -v n "%04d" $i
    wget "http://www.capitol.tn.gov/Bills/110/Bill/HB${n}.pdf"
    wget "http://www.capitol.tn.gov/Bills/110/Bill/SB${n}.pdf"
done

# 109th Legislative Session
cd ../109th

for i in {1..2692}
do
    printf -v n "%04d" $i
    wget "http://www.capitol.tn.gov/Bills/109/Bill/HB${n}.pdf"
    wget "http://www.capitol.tn.gov/Bills/109/Bill/SB${n}.pdf"
done

# 108th Legislative Session
cd ../108th

for i in {1..2649}
do
    printf -v n "%04d" $i
    wget "http://www.capitol.tn.gov/Bills/108/Bill/HB${n}.pdf"
    wget "http://www.capitol.tn.gov/Bills/108/Bill/SB${n}.pdf"
done

# 107th Legislative Session
cd ../107th

for i in {1..3887}
do
    printf -v n "%04d" $i
    wget "http://www.capitol.tn.gov/Bills/107/Bill/HB${n}.pdf"
    wget "http://www.capitol.tn.gov/Bills/107/Bill/SB${n}.pdf"
done

# 106th Legislative Session
cd ../106th

for i in {1..3998}
do
    printf -v n "%04d" $i
    wget "http://www.capitol.tn.gov/Bills/106/Bill/HB${n}.pdf"
    wget "http://www.capitol.tn.gov/Bills/106/Bill/SB${n}.pdf"
done

# 105th Legislative Session
cd ../105th

for i in {1..4280}
do
    printf -v n "%04d" $i
    wget "http://www.capitol.tn.gov/Bills/105/Bill/HB${n}.pdf"
    wget "http://www.capitol.tn.gov/Bills/105/Bill/SB${n}.pdf"
done
