#!/bin/bash

# Takes bulk .csv downloads from Openstates and combines into one database
# Assumes bulk downloads exist in this directory:
cd ~/Desktop/R/legislateR/data/

# Print the first row of one .csv file, which will be the row headers
head -1 2017-06-02-tn-csv/tn_bill_actions.csv > bill_actions.csv
# Print all rows of each file (-n+2 excludes header) and appends to the file created with head
tail -n+2 -q 2017-06-02-*-csv/*_bill_actions.csv >> bill_actions.csv

head -1 2017-06-02-tn-csv/tn_bill_legislator_votes.csv > bill_legislator_votes.csv
tail -n+2 -q 2017-06-02-*-csv/*_bill_legislator_votes.csv >> bill_legislator_votes.csv

head -1 2017-06-02-tn-csv/tn_bill_sponsors.csv > bill_sponsors.csv
tail -n+2 -q 2017-06-02-*-csv/*_bill_sponsors.csv >> bill_sponsors.csv

head -1 2017-06-02-tn-csv/tn_bill_votes.csv > bill_votes.csv
tail -n+2 -q 2017-06-02-*-csv/*_bill_votes.csv >> bill_votes.csv

head -1 2017-06-02-tn-csv/tn_bills.csv > bills.csv
tail -n+2 -q 2017-06-02-*-csv/*_bills.csv >> bills.csv

head -1 2017-06-02-tn-csv/tn_committees.csv > committees.csv
tail -n+2 -q 2017-06-02-*-csv/*_committees.csv >> committees.csv

head -1 2017-06-02-tn-csv/tn_legislator_roles.csv > legislator_roles.csv
tail -n+2 -q 2017-06-02-*-csv/*_legislator_roles.csv >> legislator_roles.csv

head -1 2017-06-02-tn-csv/tn_legislators.csv > legislators.csv
tail -n+2 -q 2017-06-02-*-csv/*_legislators.csv >> legislators.csv
