#!/bin/bash

# Mass convert pdfs downloaded using get_bill_text.sh to text files
# Requires installation of pdftotext (http://www.xpdfreader.com/)

mkdir ~/Desktop/R/legislateR/data/bills_text/{105..110}th

cd ~/Desktop/R/legislateR/data/bills_pdf/110th

for i in {1..1462}
do
    printf -v n "%04d" $i
    pdftotext "HB${n}.pdf"
    pdftotext "SB${n}.pdf"
done

mv ~/Desktop/R/legislateR/data/bills_pdf/110th/HB{0...1}*.txt ~/Desktop/R/legislateR/data/bills_text/110th
mv ~/Desktop/R/legislateR/data/bills_pdf/110th/HB{0...1}*.txt ~/Desktop/R/legislateR/data/bills_text/110th

cd ../109th

for i in {1..2692}
do
    printf -v n "%04d" $i
    pdftotext "HB${n}.pdf"
    pdftotext "SB${n}.pdf"
done

mv ~/Desktop/R/legislateR/data/bills_pdf/109th/HB{0...2}*.txt ~/Desktop/R/legislateR/data/bills_text/109th
mv ~/Desktop/R/legislateR/data/bills_pdf/109th/SB{0...2}*.txt ~/Desktop/R/legislateR/data/bills_text/109th

cd ../108th

for i in {1..2649}
do
    printf -v n "%04d" $i
    pdftotext "HB${n}.pdf"
    pdftotext "SB${n}.pdf"
done

mv ~/Desktop/R/legislateR/data/bills_pdf/108th/HB{0...2}*.txt ~/Desktop/R/legislateR/data/bills_text/108th
mv ~/Desktop/R/legislateR/data/bills_pdf/108th/SB{0...2}*.txt ~/Desktop/R/legislateR/data/bills_text/108th

cd ../107th

for i in {1..3887}
do
    printf -v n "%04d" $i
    pdftotext "HB${n}.pdf"
    pdftotext "SB${n}.pdf"
done

mv ~/Desktop/R/legislateR/data/bills_pdf/107th/HB{0...3}*.txt ~/Desktop/R/legislateR/data/bills_text/107th
mv ~/Desktop/R/legislateR/data/bills_pdf/107th/SB{0...3}*.txt ~/Desktop/R/legislateR/data/bills_text/107th

cd ../106th

for i in {1..3998}
do
    printf -v n "%04d" $i
    pdftotext "HB${n}.pdf"
    pdftotext "SB${n}.pdf"
done

mv ~/Desktop/R/legislateR/data/bills_pdf/106th/HB{0...3}*.txt ~/Desktop/R/legislateR/data/bills_text/106th
mv ~/Desktop/R/legislateR/data/bills_pdf/106th/SB{0...3}*.txt ~/Desktop/R/legislateR/data/bills_text/106th

cd ../105th

for i in {1..4280}
do
    printf -v n "%04d" $i
    pdftotext "HB${n}.pdf"
    pdftotext "SB${n}.pdf"
done

mv ~/Desktop/R/legislateR/data/bills_pdf/105th/HB{0...4}*.txt ~/Desktop/R/legislateR/data/bills_text/105th
mv ~/Desktop/R/legislateR/data/bills_pdf/105th/SB{0...4}*.txt ~/Desktop/R/legislateR/data/bills_text/105th
