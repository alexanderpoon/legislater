library(tidyverse)
library(cleanNLP)
library(caret)
library(doParallel)

# Point to Python executable
reticulate::use_python("/Users/alexanderpoon/anaconda3/bin/python")
cnlp_init_spacy()

# Use parallelism
cl <- makeCluster(4)
registerDoParallel(cl)

bill_actions <- read_csv("data/2017-06-02-tn-csv/tn_bill_actions.csv") %>%
    filter(session != 106,
        grepl("Signed by Governor.|Returned by Governor without signature.", action)) %>%
    transmute(bill_id = paste(session, bill_id), passed = 1L) %>%
    distinct()

corpus_text <- readtext::readtext("data/bills_text/*/*.txt") %>%
    mutate(doc_id = paste(str_sub(doc_id, 1, 3), str_extract(doc_id, "HB|SB"), as.numeric(str_sub(doc_id, 9, 12))),
        text = str_replace_all(text, "\\n", " "),
        text = str_replace_all(text, "HB[0-9]+|SB[0-9]+", ""),
        text = str_replace_all(text, "[0-9]{6,8}", ""),
        text = str_replace_all(text, "-[0-9]-", ""),
        text = str_replace_all(text, "HOUSE BILL [0-9]+ By|SENATE BILL [0-9]+ By", ""),
        text = str_replace_all(text, "AN ACT to amend Tennessee Code Annotated.+, relative to", ""),
        text = str_replace_all(text, "AN ACT to amend Tennessee Code Annotated.+, to", ""),
        text = str_replace_all(text, "AN ACT to amend Tennessee Code Annotated, Title [0-9]+\\.", ""),
        text = str_replace_all(text, "AN ACT to amend Chapter.+, relative to", ""),
        text = str_replace_all(text, "BE IT ENACTED BY THE GENERAL ASSEMBLY OF THE STATE OF TENNESSEE:", ""),
        text = str_replace_all(text, "<BillNo> <Sponsor>", ""),
        text = str_replace_all(text, "SECTION [0-9].", ""),
        text = str_replace_all(text, "This act shall take effect.+, the public welfare requiring it.", ""))

annotation <- cnlp_annotate(corpus_text$text, doc_ids = corpus_text$doc_id)

# Drop and modify tokens depending of part of speech
# https://spacy.io/api/annotation#pos-tagging
annotation_df <- cnlp_get_tif(annotation) %>%
    filter(pos %in% c("NN", "NNS", "NNP", "NNPS", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ"),
        !grepl("\\W|[0-9]", word),
        str_length(word) > 1,
        !word %in% stopwords::data_stopwords_snowball$en) %>%
    mutate(word = if_else(pos %in% c("NNS", "NNPS", "VBD", "VBG", "VBN", "VBP", "VBZ"), lemma, word),
        word = tolower(word)) %>%
    group_by(word) %>%
    mutate(bills_with_word = length(unique(id))) %>%
    ungroup() %>%
    filter(bills_with_word >= 5 & bills_with_word < 4000)

tfidf_matrix <- annotation_df %>%
    cnlp_get_tfidf(min_df = 0, type = "tfidf", tf_weight = "raw", idf_weight = "smooth",
        token_var = "word", doc_var = "id") %>%
    as.matrix() %>%
    as_data_frame(rownames = "bill_id") %>%
    left_join(bill_actions, by = "bill_id") %>%
    mutate(passed = factor(if_else(is.na(passed), 0L, passed)))

train_set <- filter(tfidf_matrix, substr(bill_id, 1, 3) != "110") %>% select(-bill_id) %>% distinct()
test_set <- filter(tfidf_matrix, substr(bill_id, 1, 3) == 110) %>% select(-bill_id) %>% distinct()

ctrl <- trainControl(method = "cv", number = 10)

# glmnet
model_glmnet <- train(passed ~ ., data = train_set, method = "glmnet", family = "binomial",
    trControl = ctrl, tuneLength = 10)

test_set$probs_glmnet <- predict(model_glmnet, test_set, type = "prob")$`1`
ggplot(test_set, aes(x = passed, y = probs_glmnet)) + geom_jitter() + theme_minimal()

predictions_glmnet <- predict(model_glmnet, test_set)

confusionMatrix(predictions_glmnet, test_set$passed)

write_rds(model_glmnet, "data/models/text_tfidf_glmnet.rds")

# SVM with a Polynomial Kernel
model_svm <- train(passed ~ ., data = train_set, method = "svmPoly",
    trControl = ctrl)

preds_svm <- predict(model_svm, test_set)

confusionMatrix(preds_svm, test_set$passed)

write_rds(model_svm, "data/models/text_tfidf_svm.rds")

stopCluster(cl)

# SVM with a RBF Kernel
model_rbf <- train(passed ~ ., data = train_set, method = "svmRadial",
    trControl = ctrl)

preds_rbf <- predict(model_rbf, test_set)

confusionMatrix(preds_rbf, test_set$passed)

# K-Nearest Neighbors
model_knn <- train(passed ~ ., data = train_set, method = "knn",
    trControl = ctrl, tuneLength = 20)

preds_knn <- predict(model_knn, test_set)

confusionMatrix(preds_knn, test_set$passed)

# Random Forest
model_rf <- train(passed ~ ., data = train_set, method = "ranger",
    trControl = ctrl, tuneLength = 5)

preds_rf <- predict(model_rf, test_set)

confusionMatrix(preds_rf, test_set$passed)
