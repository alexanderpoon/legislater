## R

R code for reproducibility.

To reproduce, install the packages in `installs.R`. The `cleanNLP` spaCy backend requires a Python installation. Instructions for that installation [here](https://github.com/statsmaths/cleanNLP).
