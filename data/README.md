## data

Placeholder for OpenStates data.

Download TN bulk downloads from [OpenStates](https://openstates.org/downloads/) into this folder to run [R](https://github.com/alexander-poon/legislateR/tree/master/R) scripts and [Python](https://github.com/alexander-poon/legislateR/tree/master/py) notebooks.
