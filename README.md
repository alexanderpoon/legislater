## Motivation

Exploring State Legislation with NLP. Inspired by a discussion at a [Code for Nashville](https://github.com/code-for-nashville) brigade meeting. Data comes via [OpenStates](https://openstates.org).

A project for me to learn Python, NLP, and ML, and get familiarized with my state's legislative process.

Guiding questions:

1. Can we identify frequent topics of legislation for legislators/states?
2. Can we identify legislator/state positions on certain topics of legislation?
3. Given observable information about a bill when it is introduced, can we predict its probability of getting passed?

## Identifying Interests by Legislator

I do this by treating the set of bills introduced by each legislator as a document, and extracting terms with the highest values of [TF-IDF](https://en.wikipedia.org/wiki/Tf%E2%80%93idf) by legislator.

## Predicting Bill Passage

I approach this as a binary classification task (pass/not pass).

**Data**

I have two sources of text: a `title` field from OpenStates data (viz. a one sentence summary of each bill), and full bill text from the [Tennessee General Assembly](http://www.capitol.tn.gov/legislation/archives.html) website.

**Preprocessing and Feature Engineering**

I use the `spaCy` annotator for part of speech tagging, lemmatization, and named entity recognition. I filter the text on parts of speech, then use `gensim` for n-gram detection.

In building predictive models, I alternatively use TF-IDF or word embeddings. I alternatively use pretrained `GloVe` word vectors available via `spaCy` and train my own word2vec word embeddings.

I represent each bill as the average of its component word vectors, weighted by TF-IDF.

**Models**

I use the following models:

- Naive Bayes (for TF-IDF)
- Logistic Regression (for word embeddings) and `glmnet` (for TF-IDF)
- SVM
- Nearest Neighbors
- Gradient Boosting

To mimic prediction of future bills, I train models with bills from all but the most recent legislative session, and evaluate predictions using the most recent session.

**Results**

See the notebooks in [py/](https://github.com/alexander-poon/legislateR/tree/master/py).

## To Do

- Classification with Deep Learning
- Semantic Search
